package com.dbserver.db.ServicesTest;

import com.dbserver.db.Controllers.Login;
import com.dbserver.db.Objects.*;
import com.dbserver.db.Services.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTest {
    @Mock
    ClientService cs;
    Client newClient;
    EventService es;
    Event newEvent;
    StaffService ss;
    Staff newStaff;
    Staff newContact;
    InternalStaff IS;
    Contact contact;
    LoginService ls;

    @Before
    public void init() {
        cs = new ClientService();
        es = new EventService();
        ss = new StaffService();

        newClient = new Client();
        newClient.setPw("");
        newClient.setUuid("");
        newClient.setSubscribed(false);
        newClient.setFirst_name("Unit");
        newClient.setLast_name("Test");
        newClient.setEmail_address("Unit.Test@hotmail.com");
        newClient.setStreet_no("01");
        newClient.setStreet_name("Test Street");
        newClient.setTown("Sittingbourne");
        newClient.setCounty("Kent");
        newClient.setPost_code("ME10 1DS");
        newClient = cs.accessDB(newClient);

        ls = new LoginService();
        try {
            newClient = ls.clientLogin(newClient.getEmail_address(), newClient.getPw());
            Assert.assertTrue(newClient.getClient_ID() > 0 & !newClient.getUuid().equals(""));
        } catch (Exception e) {
            e.printStackTrace();
        }

        newStaff = new Staff();
        newStaff.setStaff_ID(0);
        newStaff.setFirst_name("Test");
        newStaff.setLast_name("InternalStaff");
        newStaff.setEmail_address("TestStaff10.InternalStaff@estate.com");
        newStaff.setPw("");
        newStaff.setLogged_in(0);
        newStaff.setSessionUUID("");
        newStaff.setInternal(true);
        newStaff = ss.createStaff(newStaff);

        IS = new InternalStaff();
        IS.setStaff_ID(newStaff.getStaff_ID());
        IS.setAdmin(true);
        IS.setCreate_rights(true);
        IS.setRead_rights(true);
        IS.setAmend_rights(true);
        IS.setDelete_rights(true);
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(IS, Map.class);
        ss.updateStaffDetails(map, "internal_staff_TBL");

        try {
            newStaff = ls.staffLogin(newStaff.getEmail_address(), newStaff.getPw());
            Assert.assertTrue(newStaff.getStaff_ID() > 0 & !newStaff.getSessionUUID().equals(""));
        } catch (Exception e) {
            e.printStackTrace();
        }

        newContact = new Staff();
        newContact.setStaff_ID(0);
        newContact.setFirst_name("Test");
        newContact.setLast_name("Contact");
        newContact.setEmail_address("TestContactStaff10.TestContact@estate.com");
        newContact.setPw("");
        newContact.setLogged_in(0);
        newContact.setSessionUUID("");
        newContact.setInternal(false);
        newContact = ss.createStaff(newContact);

        contact = new Contact();
        contact.setCompany_name("Unit testing");
        contact.setHourly_rate("10.00");
        contact.setStaff_ID(newContact.getStaff_ID());
        ObjectMapper contactmapper = new ObjectMapper();
        Map<String, Object> contactmap = contactmapper.convertValue(contact, Map.class);
        ss.updateStaffDetails(contactmap, "Contact_TBL");

        try {
            newContact = ls.staffLogin(newContact.getEmail_address(), newContact.getPw());
            Assert.assertTrue(newContact.getStaff_ID() > 0 & !newContact.getSessionUUID().equals(""));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void delete() {
        if ( !newClient.getClient_ID().equals(0) ) {
            cs.deleteClient(newClient.getClient_ID());
        }
        if ( !contact.getStaff_ID().equals(0) ) {
            ss.deleteStaff(contact.getStaff_ID(), false, contact.getStaff_ID(), false);
        }
        if ( !IS.getStaff_ID().equals(0) ) {
            ss.deleteStaff(IS.getStaff_ID(), true, IS.getStaff_ID(), true);
        }
    }


    @Test
    public void createClientAndCheckClient() {
        Assert.assertTrue(newClient.getClient_ID() > 0);

        try {
            newClient = ls.loggedInCheck(newClient.getUuid());
            Assert.assertTrue(newClient.getClient_ID() > 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertTrue(cs.deleteClient(newClient.getClient_ID()).equals("deleted"));
        newClient.setClient_ID(0);
    }

    @Test
    public void createAndCheckInternalStaff() {
        Assert.assertTrue(newStaff.getStaff_ID() > 0);

        try {
            newStaff = ls.staffLoggedInCheck(newStaff.getSessionUUID());
            Assert.assertTrue(newStaff.getStaff_ID() > 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Staff> staffList = ss.getAllStaff();
        Assert.assertTrue(staffList.get(staffList.size() - 2).getStaff_ID().equals(newStaff.getStaff_ID()));

        List<Staff> internalStaffList = ss.getStaffBasedOnInternal(newStaff.getStaff_ID(), newStaff.getSessionUUID(), 0, 1000);
        Assert.assertTrue(internalStaffList.get(internalStaffList.size() - 2).getStaff_ID().equals(newStaff.getStaff_ID()));

        Integer staffID = newStaff.getStaff_ID();
        newStaff = ss.getStaffById(staffID);
        Assert.assertTrue(newStaff.getStaff_ID().equals(staffID));
        IS = (InternalStaff) ss.getStaffDetails(true, newStaff.getStaff_ID(), newStaff.getSessionUUID(), newStaff.getStaff_ID(), true);

        Assert.assertTrue(IS.getStaff_ID().equals(newStaff.getStaff_ID()));

        Assert.assertTrue(ss.deleteStaff(newStaff.getStaff_ID(), true, newStaff.getStaff_ID(), true).equals("Staff member deleted"));
        IS.setStaff_ID(0);
    }

    @Test
    public void createAndCheckContact() {
        Assert.assertTrue(newContact.getStaff_ID() > 0);

        try {
            newContact = ls.staffLoggedInCheck(newContact.getSessionUUID());
            Assert.assertTrue(newContact.getStaff_ID() > 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Staff> staffList = ss.getAllStaff();
        Assert.assertTrue(staffList.get(staffList.size() - 1).getStaff_ID().equals(newContact.getStaff_ID()));

        Integer staffID = newContact.getStaff_ID();
        newContact = ss.getStaffById(staffID);
        Assert.assertTrue(newContact.getStaff_ID().equals(staffID));
        contact = (Contact) ss.getStaffDetails(false, newContact.getStaff_ID(), newContact.getSessionUUID(), newContact.getStaff_ID(), false);

        Assert.assertTrue(contact.getStaff_ID().equals(newContact.getStaff_ID()));

        Assert.assertTrue(ss.deleteStaff(newContact.getStaff_ID(), false, newContact.getStaff_ID(), false).equals("Staff member deleted"));
        contact.setStaff_ID(0);
    }

    @Test
    public void createAndCheckEvents() {
        Event newEvent = new Event();
        newEvent.setEvent_ID(0);
        newEvent.setClients_attending("");
        newEvent.setImages("");
        newEvent.setLocation("");
        newEvent.setEvent_description("This is an event");
        newEvent.setEvent_name("Selling event");
        newEvent.setStart_time(new Date().getTime() + 200000);
        newEvent.setEnd_time(new Date().getTime() + 400000);
        newEvent.setEvent_host_ID(newContact.getStaff_ID());
        newEvent.setStaff_manager_ID(null);
        newEvent.setStreet_no("02");
        newEvent.setStreet_name("Event road");
        newEvent.setTown("Event town");
        newEvent.setCounty("Event county");
        newEvent.setPost_code("ME10 1JS");
        newEvent.setPrice("20.00");

        newEvent = es.createEvent(newEvent, newContact.getStaff_ID(), newContact.getSessionUUID());
        Assert.assertTrue(newEvent.getEvent_ID() > 0);

        List<Event> eventList1 = es.getUnassignedEvents(newStaff.getStaff_ID(), newStaff.getSessionUUID(), 0, 1000);
        Assert.assertTrue(eventList1.get(eventList1.size() - 1).getEvent_ID().equals(newEvent.getEvent_ID()));

        newEvent = es.addStaffManagerToEvent(newStaff.getStaff_ID(), newStaff.getSessionUUID(), newEvent.getEvent_ID());
        Assert.assertTrue(newEvent.getEvent_ID() > 0 & newEvent.getStaff_manager_ID().equals(newStaff.getStaff_ID()));

        List<Event> eventList3 = es.getUpcomingEvents(newStaff.getStaff_ID(), newStaff.getSessionUUID(), newStaff.getInternal(), 0, 1000);
        Assert.assertTrue(eventList3.get(eventList3.size() - 1).getEvent_ID().equals(newEvent.getEvent_ID()));

        List<Event> eventList4 = es.getAuthEventsRange(0, 1000, new Date().getTime(), "10000000", "0", "", "", newClient.getClient_ID());
        Assert.assertTrue(eventList4.get(eventList4.size() - 1).getEvent_ID().equals(newEvent.getEvent_ID()));

        newEvent = es.addClientToEvent(newEvent.getEvent_ID(), newClient.getClient_ID(), newClient.getUuid());
        Assert.assertTrue(newEvent.getClients_attending().equals(newClient.getClient_ID().toString()));

        Assert.assertTrue(es.deleteEvent(newContact.getStaff_ID(), newContact.getSessionUUID(), newEvent.getEvent_ID(), newContact.getInternal()).equals("Event deleted"));
    }

    @Test
    public void createAndCheckProperties() {
        Property newProperty = new Property();
        newProperty.setProperty_ID(0);
        newProperty.setStreet_no("03");
        newProperty.setStreet_name("Property lane");
        newProperty.setTown("New town");
        newProperty.setCounty("Property Area");
        newProperty.setPost_code("ME10 1TY");
        newProperty.setLocation("");
        newProperty.setDescription("THis is a property");
        newProperty.setPrice("200000");
        newProperty.setImages("");
        newProperty.setAttachments("");

        PropertyService ps = new PropertyService();
        newProperty = ps.createProperty(newProperty);
        Assert.assertTrue(newProperty.getProperty_ID() > 0);

        Sale newSale = new Sale();
        newSale.setStaff_ID(newStaff.getStaff_ID());
        newSale.setSale_price("");
        newSale.setBuyer_ID(0);
        newSale.setSeller_ID(newClient.getClient_ID());
        newSale.setProperty_ID(newProperty.getProperty_ID());
        newSale.setSale_ID(0);

        SaleService saleService = new SaleService();

        newSale = saleService.createSale(newSale);
        Assert.assertTrue(newSale.getProperty_ID().equals(newProperty.getProperty_ID()));

        List<Property> propertyList = ps.getAvailablePropertyRange(0, 1000, "300000", "100000", "", "");
        Assert.assertTrue(propertyList.get(propertyList.size() - 1).getProperty_ID().equals(newProperty.getProperty_ID()));

        String deletedSale = saleService.deleteSale(newSale.getSale_ID());
        Assert.assertTrue(deletedSale.equals("Sale deleted"));

        String deletedProperty = ps.deleteProperty(newProperty.getProperty_ID());
        Assert.assertTrue(deletedProperty.equals("Property deleted"));
    }
}
