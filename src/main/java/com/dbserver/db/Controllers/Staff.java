package com.dbserver.db.Controllers;

import com.dbserver.db.Objects.Contact;
import com.dbserver.db.Objects.InternalStaff;
import com.dbserver.db.Services.StaffService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController("/uk/AddStaff")
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class Staff {
    @PostMapping(value="/uk/AddStaff", consumes="application/json")
    public ResponseEntity index(@RequestBody com.dbserver.db.Objects.Staff sStaff) {
        StaffService staffService = new StaffService();
        com.dbserver.db.Objects.Staff newStaff = staffService.createStaff(sStaff);

        return new ResponseEntity(newStaff, HttpStatus.OK);
    }


    @RequestMapping("/uk/GetAllStaff")
    public ResponseEntity getAllStaff() {
        StaffService staffService = new StaffService();
        List<com.dbserver.db.Objects.Staff> staffList = staffService.getAllStaff();

        return new ResponseEntity(staffList, HttpStatus.OK);
    }

    @RequestMapping("/uk/GetStaffBasedOnInternal")
    public ResponseEntity getStaffBasedOnInternal(@RequestHeader Integer staff_ID, @RequestHeader String uuid, @RequestHeader Integer start, @RequestHeader Integer amount) {
        if ( amount > 25 ) {
            amount = 25;
        }

        StaffService staffService = new StaffService();
        List<com.dbserver.db.Objects.Staff> staffList = staffService.getStaffBasedOnInternal(staff_ID, uuid, start, amount);

        return new ResponseEntity(staffList, HttpStatus.OK);
    }

    @RequestMapping("/uk/GetStaffDetails")
    public ResponseEntity getStaffDetails(@RequestHeader Integer staff_ID, @RequestHeader String uuid, @RequestHeader Boolean internalType, @RequestHeader Integer search_ID, @RequestHeader Boolean searchInternalType) {
        StaffService staffService = new StaffService();
        Object staffDetails = staffService.getStaffDetails(internalType, staff_ID, uuid, search_ID, searchInternalType);

        return new ResponseEntity(staffDetails, HttpStatus.OK);
    }

    @RequestMapping("/uk/UpdateStaffDetails")
    public ResponseEntity updateStaffDetails(@RequestBody com.dbserver.db.Objects.Staff body) {
        StaffService staffService = new StaffService();
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(body, Map.class);
        staffService.updateStaffDetails(map, "Staff_TBL");

        return new ResponseEntity("Staff member updated", HttpStatus.OK);
    }

    @RequestMapping("/uk/UpdateInternalStaffDetails")
    public ResponseEntity updateInternalStaffDetails(@RequestBody InternalStaff body) {
        StaffService staffService = new StaffService();
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(body, Map.class);
        staffService.updateStaffDetails(map, "internal_staff_TBL");

        return new ResponseEntity("Staff member updated", HttpStatus.OK);
    }

    @RequestMapping("/uk/UpdateContactDetails")
    public ResponseEntity updateContactDetails(@RequestBody Contact body) {
        StaffService staffService = new StaffService();
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(body, Map.class);
        staffService.updateStaffDetails(map, "Contact_TBL");

        return new ResponseEntity("Staff member updated", HttpStatus.OK);
    }

    @RequestMapping("/uk/DeleteStaff")
    public ResponseEntity deleteStaff(@RequestHeader Integer staff_ID, @RequestHeader Boolean internalType, @RequestHeader Integer delete_ID, @RequestHeader Boolean deleteType) {
        StaffService staffService = new StaffService();
        String retVal = staffService.deleteStaff(staff_ID, internalType, delete_ID, deleteType);

        return new ResponseEntity(retVal, HttpStatus.OK);
    }
}
