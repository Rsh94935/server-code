package com.dbserver.db.Controllers;

import com.dbserver.db.Services.ClientService;
import com.dbserver.db.Services.PropertyService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController("/uk/AddProperty")
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class Property {
    @GetMapping(value="/uk/GetAPIKey", consumes="application/json")
    public ResponseEntity getKey() {
        return new ResponseEntity("Bearer ya29.a0AfH6SMAZEffGjwLEsly0dl3WAWP3qHGH5BVPau77N7-pU_ydyeHApyqtd8upu8DhvEQTf7-au1S91x-7WdmywMpEDPcwqg5W3wINJcNnlv_J0lxVkhULMG1m6JLL7vGASe1BTi_tBLdHqn8UrdeYsyaYSUKFa_FaJK4", HttpStatus.OK);
    }


    @PostMapping(value="/uk/AddProperty", consumes="application/json")
    public ResponseEntity createProperty(@RequestBody com.dbserver.db.Objects.Property sProperty) {
        PropertyService propertyService = new PropertyService();
        com.dbserver.db.Objects.Property newProp = propertyService.createProperty(sProperty);

        return new ResponseEntity(newProp, HttpStatus.OK);
    }

    @GetMapping(value="/uk/GetAllProperties", consumes="application/json")
    public ResponseEntity getAllProperties() {
        PropertyService propertyService = new PropertyService();
        List<com.dbserver.db.Objects.Property> propertyList = propertyService.getAllProperties();

        return new ResponseEntity(propertyList, HttpStatus.OK);
    }

    @GetMapping(value="/uk/GetPropertyRange", consumes="application/json")
    public ResponseEntity getPropertyRange(@RequestHeader Integer from, @RequestHeader Integer to, @RequestHeader String hPrice, @RequestHeader String lPrice, @RequestHeader String locationType, @RequestHeader String searchLocation) {
        PropertyService propertyService = new PropertyService();
        List<com.dbserver.db.Objects.Property> propertyList = propertyService.getPropertyRange(from, to, hPrice, lPrice, locationType, searchLocation);

        return new ResponseEntity(propertyList, HttpStatus.OK);
    }

    @GetMapping(value="/uk/GetAvailablePropertyRange", consumes="application/json")
    public ResponseEntity getAvailablePropertyRange(@RequestHeader Integer from, @RequestHeader Integer to, @RequestHeader String hPrice, @RequestHeader String lPrice, @RequestHeader String locationType, @RequestHeader String searchLocation) {
        PropertyService propertyService = new PropertyService();
        List<com.dbserver.db.Objects.Property> propertyList = propertyService.getAvailablePropertyRange(from, to, hPrice, lPrice, locationType, searchLocation);

        return new ResponseEntity(propertyList, HttpStatus.OK);
    }

    @PostMapping(value="/uk/AmendProperty", consumes="application/json")
    public ResponseEntity amendProperty(@RequestBody com.dbserver.db.Objects.Property sProperty) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(sProperty, Map.class);
        PropertyService propertyService = new PropertyService();
        propertyService.updatePropertyDetails(map);

        return new ResponseEntity("Property amended", HttpStatus.OK);
    }

    @DeleteMapping(value="/uk/DeleteProperty", consumes="application/json")
    public ResponseEntity deleteProperty(@RequestHeader Integer propertyId) {
        PropertyService propertyService = new PropertyService();
        propertyService.deleteProperty(propertyId);

        return new ResponseEntity("Property deleted", HttpStatus.OK);
    }

}
