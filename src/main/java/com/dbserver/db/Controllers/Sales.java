package com.dbserver.db.Controllers;

import com.dbserver.db.Objects.Event;
import com.dbserver.db.Objects.Sale;
import com.dbserver.db.Services.ClientService;
import com.dbserver.db.Services.EventService;
import com.dbserver.db.Services.PropertyService;
import com.dbserver.db.Services.SaleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController("/uk/AddSale")
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class Sales {
    @PostMapping(value="/uk/AddSale", consumes="application/json")
    public ResponseEntity createSale(@RequestBody Sale sSale, @RequestHeader Integer staff_ID) {
        SaleService saleService = new SaleService();
        saleService.createSale(sSale);

        return new ResponseEntity("Sale created", HttpStatus.OK);
    }

    @GetMapping(value="/uk/GetSale", consumes="application/json")
    public ResponseEntity getSale(@RequestHeader Integer saleId) {
        SaleService saleService = new SaleService();
        Sale sale = saleService.getSale(saleId);

        return new ResponseEntity(sale, HttpStatus.OK);
    }

    @GetMapping(value="/uk/GetMySales")
    public ResponseEntity getMySales(@RequestHeader Integer staff_ID, @RequestHeader String uuid, @RequestHeader Integer start, @RequestHeader Integer amount){
        if ( amount > 25 ) {
            amount = 25;
        } else if ( amount < 1 ) {
            amount = 1;
        }
        SaleService ss = new SaleService();
        List<Sale> sales = ss.getMySales(staff_ID, uuid, start, amount);

        return new ResponseEntity(sales, HttpStatus.OK);
    }

    @PostMapping(value="/uk/UpdateSale", consumes="application/json")
    public ResponseEntity updateSale(@RequestBody Sale sSale) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(sSale, Map.class);
        SaleService saleService = new SaleService();
        saleService.updateSaleDetails(map);

        return new ResponseEntity("Sale amended", HttpStatus.OK);
    }

    @DeleteMapping(value="/uk/DeleteSale", consumes="application/json")
    public ResponseEntity deleteSale(@RequestHeader Integer saleID) {
        SaleService saleService = new SaleService();
        saleService.deleteSale(saleID);

        return new ResponseEntity("Sale Deleted", HttpStatus.OK);
    }

}
