package com.dbserver.db.Controllers;

import com.dbserver.db.Objects.Client;
import com.dbserver.db.Objects.Staff;
import com.dbserver.db.Services.LoginService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;


@RestController("/uk/ClientLogin")
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class Login {
    @GetMapping(value="/uk/ClientLogin", consumes="application/json")
    public ResponseEntity index(@RequestHeader String username, @RequestHeader String pw) {
        LoginService ls = new LoginService();
        Client loggedInClient = new Client();
        try {
            loggedInClient = ls.clientLogin(username, pw);
        } catch (Exception e) {
            return new ResponseEntity(loggedInClient, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(loggedInClient, HttpStatus.OK);
    }

    @GetMapping(value="/uk/ClientSessionStatus", consumes="application/json")
    public ResponseEntity checkSessionLogout(@RequestHeader String uuid) {
        LoginService ls = new LoginService();
        Client sessionStatus = null;
        try {
            sessionStatus = ls.loggedInCheck(uuid);
        } catch (Exception e) {
            return new ResponseEntity("User not logged in", HttpStatus.OK);
        }

        return new ResponseEntity(sessionStatus, HttpStatus.OK);
    }
    @GetMapping(value="/uk/StaffLogin", consumes="application/json")
    public ResponseEntity staffLogin(@RequestHeader String username, @RequestHeader String pw) {
        LoginService ls = new LoginService();
        Staff loggedInStaff = null;
        try {
            loggedInStaff = ls.staffLogin(username, pw);
        } catch (Exception e) {
            return new ResponseEntity("User not found", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(loggedInStaff, HttpStatus.OK);
    }

    @GetMapping(value="/uk/StaffSessionStatus", consumes="application/json")
    public ResponseEntity checkStaffSessionLogout(@RequestHeader String uuid) {
        LoginService ls = new LoginService();
        Staff sessionStatus = new Staff();
        try {
            sessionStatus = ls.staffLoggedInCheck(uuid);
        } catch (Exception e) {
            return new ResponseEntity(sessionStatus, HttpStatus.OK);
        }

        return new ResponseEntity(sessionStatus, HttpStatus.OK);
    }

}
