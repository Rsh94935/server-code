package com.dbserver.db.Controllers;

import com.dbserver.db.Services.EventService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController("/uk/AddEvent")
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class Event {
    @PostMapping(value="/uk/AddEvent", consumes="application/json")
    public ResponseEntity createEvent(@RequestBody com.dbserver.db.Objects.Event sEvent, @RequestHeader Integer staff_ID, @RequestHeader String uuid) {
        EventService es = new EventService();
        com.dbserver.db.Objects.Event event = es.createEvent(sEvent, staff_ID, uuid);

        return new ResponseEntity(event, HttpStatus.OK);
    }

    @RequestMapping(value="/uk/AddManager", consumes="application/json")
    public ResponseEntity addManager(@RequestHeader Integer staff_ID, @RequestHeader String uuid, @RequestHeader Integer event_ID) {
        EventService es = new EventService();
        com.dbserver.db.Objects.Event response = es.addStaffManagerToEvent(staff_ID, uuid, event_ID);

        return new ResponseEntity(response, HttpStatus.OK);
    }

    @RequestMapping(value="/uk/AddAttendee", consumes="application/json")
    public ResponseEntity addAttendee(@RequestHeader Integer event_ID, @RequestHeader Integer client_ID, @RequestHeader String uuid) {
        EventService es = new EventService();
        com.dbserver.db.Objects.Event event = es.addClientToEvent(event_ID, client_ID, uuid);

        return new ResponseEntity(event, HttpStatus.OK);
    }

    @GetMapping(value="/uk/GetEventsRange", consumes="application/json")
    public ResponseEntity getEventsRange(@RequestHeader Integer start, @RequestHeader Integer amount, @RequestHeader long currentDate) {
        if ( amount > 25 ) {
            amount = 25;
        } else if ( amount < 1 ) {
            amount = 1;
        }
        EventService es = new EventService();
        List<com.dbserver.db.Objects.Event> events = es.getEventsRange(start, amount, currentDate);

        return new ResponseEntity(events, HttpStatus.OK);
    }

    @GetMapping(value="/uk/GetAuthEventsRange", consumes="application/json")
    public ResponseEntity getAuthEventsRange(@RequestHeader Integer start, @RequestHeader Integer amount, @RequestHeader long currentDate, @RequestHeader String hPrice, @RequestHeader String lPrice, @RequestHeader String locationType, @RequestHeader String searchLocation, @RequestHeader Integer client_ID) {
        if ( amount > 25 ) {
            amount = 25;
        } else if ( amount < 1 ) {
            amount = 1;
        }
        EventService es = new EventService();
        List<com.dbserver.db.Objects.Event> events = es.getAuthEventsRange(start, amount, currentDate, hPrice, lPrice, locationType, searchLocation, client_ID);

        return new ResponseEntity(events, HttpStatus.OK);
    }

    @RequestMapping(value="/uk/GetEvents", consumes="application/json")
    public ResponseEntity getEvents(@RequestHeader String location, @RequestHeader String locationName, @RequestHeader Integer start, @RequestHeader Integer amount) {
        if ( amount > 25 ) {
            amount = 25;
        } else if ( amount < 1 ) {
            amount = 1;
        }
        EventService es = new EventService();
        List<com.dbserver.db.Objects.Event> events = new ArrayList<>();
        if (location.equals("county")) {
            events = es.getEventsInCounty(locationName, start, amount);
        } else {
            events = es.getEventsInTown(locationName, start, amount);
        }

        return new ResponseEntity(events, HttpStatus.OK);
    }

    @RequestMapping(value="/uk/GetUpcomingEvents", consumes="application/json")
    public ResponseEntity getUpcomingEvents(@RequestHeader Integer staff_ID, @RequestHeader String uuid, @RequestHeader Boolean internalType, @RequestHeader Integer start, @RequestHeader Integer amount) {
        if ( amount > 25 ) {
            amount = 25;
        } else if ( amount < 1 ) {
            amount = 1;
        }
        EventService es = new EventService();
        List<com.dbserver.db.Objects.Event> events = es.getUpcomingEvents(staff_ID, uuid, internalType, start, amount);

        return new ResponseEntity(events, HttpStatus.OK);
    }

    @RequestMapping(value="/uk/GetUnassignedEvents", consumes="application/json")
    public ResponseEntity getUnassignedEvents(@RequestHeader Integer staff_ID, @RequestHeader String uuid, @RequestHeader Integer start, @RequestHeader Integer amount) {
        if ( amount > 25 ) {
            amount = 25;
        } else if ( amount < 1 ) {
            amount = 1;
        }
        EventService es = new EventService();
        List<com.dbserver.db.Objects.Event> events = es.getUnassignedEvents(staff_ID, uuid, start, amount);

        return new ResponseEntity(events, HttpStatus.OK);
    }

    @RequestMapping(value="/uk/AmendEvent", consumes="application/json")
    public ResponseEntity amendEvent(@RequestBody com.dbserver.db.Objects.Event sEvent, @RequestHeader Integer staff_ID, @RequestHeader String uuid) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(sEvent, Map.class);
        EventService es = new EventService();
        String response = es.updateEvent(map, staff_ID, uuid);

        return new ResponseEntity(response, HttpStatus.OK);
    }

    @DeleteMapping(value="/uk/DeleteEvent", consumes="application/json")
    public ResponseEntity deleteEvent(@RequestHeader Integer event_ID, @RequestHeader Integer staff_ID, @RequestHeader String uuid, @RequestHeader Boolean internalType) {
        EventService es = new EventService();
        String response = es.deleteEvent(staff_ID, uuid, event_ID, internalType);

        return new ResponseEntity(response, HttpStatus.OK);
    }
}