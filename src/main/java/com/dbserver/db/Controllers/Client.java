package com.dbserver.db.Controllers;

import com.dbserver.db.Services.ClientService;
import com.dbserver.db.Services.EmailService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Email;
import java.util.List;

@RestController("/uk/AddClient")
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class Client {
    @PostMapping(value="/uk/AddClient", consumes="application/json")
    public ResponseEntity index(@RequestBody com.dbserver.db.Objects.Client sClient) {
        ClientService clientService = new ClientService();
        clientService.accessDB(sClient);

        return new ResponseEntity("User Created", HttpStatus.OK);
    }

    @RequestMapping("/uk/GetClientList")
    public ResponseEntity getClientList(@RequestHeader Integer staff_ID, @RequestHeader String uuid, @RequestHeader Integer start, @RequestHeader Integer amount) {
        if ( amount > 25 ) {
            amount = 25;
        } else if ( amount < 1 ) {
            amount = 1;
        }

        ClientService cs = new ClientService();
        List<com.dbserver.db.Objects.Client> clients = cs.getClientList(staff_ID, uuid, start, amount);

        return new ResponseEntity(clients, HttpStatus.OK);
    }

    @PostMapping(value="/uk/ContactClient", consumes="application/json")
    public ResponseEntity contactClient(@RequestHeader Integer staff_ID, @RequestHeader String uuid, @RequestHeader Integer client_ID, @RequestHeader String subject, @RequestHeader String message) {
        ClientService cs = new ClientService();
        String retVal = cs.contactClient(staff_ID, uuid, client_ID, subject, message);

        return new ResponseEntity(retVal, HttpStatus.OK);
    }

    @DeleteMapping(value="/uk/DeleteClient", consumes="application/json")
    public ResponseEntity deleteClient(@RequestHeader Integer client_ID, @RequestHeader Integer staff_ID, @RequestHeader String uuid) {
        ClientService clientService = new ClientService();
        String retVal = clientService.deleteClient(client_ID, staff_ID, uuid);

        return new ResponseEntity(retVal, HttpStatus.OK);
    }
}
