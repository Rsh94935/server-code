package com.dbserver.db.Objects;

public class Staff {
    Integer staff_ID;
    String first_name;
    String last_name;
    String email_address;
    String pw;
    Boolean internal;
    String sessionUUID;
    long logged_in;

    public Integer getStaff_ID() {
        return staff_ID;
    }

    public void setStaff_ID(Integer staff_ID) {
        this.staff_ID = staff_ID;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public Boolean getInternal() {
        return internal;
    }

    public void setInternal(Boolean internal) {
        this.internal = internal;
    }

    public String getSessionUUID() {
        return sessionUUID;
    }

    public void setSessionUUID(String sessionUUID) {
        this.sessionUUID = sessionUUID;
    }

    public long getLogged_in() {
        return logged_in;
    }

    public void setLogged_in(long logged_in) {
        this.logged_in = logged_in;
    }
}
