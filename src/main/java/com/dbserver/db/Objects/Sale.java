package com.dbserver.db.Objects;

public class Sale {
    Integer sale_ID;
    Integer property_ID;
    Integer seller_ID;
    Integer buyer_ID;
    Integer staff_ID;
    String sale_price;

    public Integer getSale_ID() {
        return sale_ID;
    }

    public void setSale_ID(Integer sale_ID) {
        this.sale_ID = sale_ID;
    }

    public Integer getProperty_ID() {
        return property_ID;
    }

    public void setProperty_ID(Integer property_ID) {
        this.property_ID = property_ID;
    }

    public Integer getSeller_ID() {
        return seller_ID;
    }

    public void setSeller_ID(Integer seller_ID) {
        this.seller_ID = seller_ID;
    }

    public Integer getBuyer_ID() {
        return buyer_ID;
    }

    public void setBuyer_ID(Integer buyer_ID) {
        this.buyer_ID = buyer_ID;
    }

    public Integer getStaff_ID() {
        return staff_ID;
    }

    public void setStaff_ID(Integer staff_ID) {
        this.staff_ID = staff_ID;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }
}
