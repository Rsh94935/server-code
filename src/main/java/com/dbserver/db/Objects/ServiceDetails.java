package com.dbserver.db.Objects;

public class ServiceDetails {
    Integer service_details_ID;
    String name;
    String description;
    String street_no;
    String street_name;
    String town;
    String county;
    String post_code;
    String price;
    String images;
    String location;
    Integer service_provider;
    long dateStart;
    long dateEnd;
    String hostingCompany;

    public Integer getService_details_ID() {
        return service_details_ID;
    }

    public void setService_details_ID(Integer service_details_ID) {
        this.service_details_ID = service_details_ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStreet_no() {
        return street_no;
    }

    public void setStreet_no(String street_no) {
        this.street_no = street_no;
    }

    public String getStreet_name() {
        return street_name;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getService_provider() {
        return service_provider;
    }

    public void setService_provider(Integer service_provider) {
        this.service_provider = service_provider;
    }

    public long getDateStart() {
        return dateStart;
    }

    public void setDateStart(long dateStart) {
        this.dateStart = dateStart;
    }

    public long getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(long dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getHostingCompany() {
        return hostingCompany;
    }

    public void setHostingCompany(String hostingCompany) {
        this.hostingCompany = hostingCompany;
    }
}
