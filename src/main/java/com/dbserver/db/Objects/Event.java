package com.dbserver.db.Objects;

public class Event {
    Integer event_ID;
    Integer staff_manager_ID;
    Integer event_host_ID;
    String clients_attending;
    long start_time;
    long end_time;
    String event_name;
    String event_description;
    String street_no;
    String street_name;
    String town;
    String county;
    String post_code;
    String price;
    String location;
    String images;

    public Integer getEvent_ID() {
        return event_ID;
    }

    public void setEvent_ID(Integer event_ID) {
        this.event_ID = event_ID;
    }

    public Integer getStaff_manager_ID() {
        return staff_manager_ID;
    }

    public void setStaff_manager_ID(Integer staff_manager_ID) {
        this.staff_manager_ID = staff_manager_ID;
    }

    public Integer getEvent_host_ID() {
        return event_host_ID;
    }

    public void setEvent_host_ID(Integer event_host_ID) {
        this.event_host_ID = event_host_ID;
    }

    public String getClients_attending() {
        return clients_attending;
    }

    public void setClients_attending(String clients_attending) {
        this.clients_attending = clients_attending;
    }

    public long getStart_time() {
        return start_time;
    }

    public void setStart_time(long start_time) {
        this.start_time = start_time;
    }

    public long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(long end_time) {
        this.end_time = end_time;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_description() {
        return event_description;
    }

    public void setEvent_description(String event_description) {
        this.event_description = event_description;
    }

    public String getStreet_no() {
        return street_no;
    }

    public void setStreet_no(String street_no) {
        this.street_no = street_no;
    }

    public String getStreet_name() {
        return street_name;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
