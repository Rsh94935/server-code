package com.dbserver.db.Objects;

public class Contact {
    Integer staff_ID;
    String company_name;
    String hourly_rate;

    public Integer getStaff_ID() {
        return staff_ID;
    }

    public void setStaff_ID(Integer staff_ID) {
        this.staff_ID = staff_ID;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getHourly_rate() {
        return hourly_rate;
    }

    public void setHourly_rate(String hourly_rate) {
        this.hourly_rate = hourly_rate;
    }
}
