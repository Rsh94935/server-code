package com.dbserver.db.Objects;

public class InternalStaff {
    Integer staff_ID;
    Boolean read_rights;
    Boolean amend_rights;
    Boolean create_rights;
    Boolean delete_rights;
    Boolean admin;

    public Integer getStaff_ID() {
        return staff_ID;
    }

    public void setStaff_ID(Integer staff_ID) {
        this.staff_ID = staff_ID;
    }

    public Boolean getRead_rights() {
        return read_rights;
    }

    public void setRead_rights(Boolean read_rights) {
        this.read_rights = read_rights;
    }

    public Boolean getAmend_rights() {
        return amend_rights;
    }

    public void setAmend_rights(Boolean amend_rights) {
        this.amend_rights = amend_rights;
    }

    public Boolean getCreate_rights() {
        return create_rights;
    }

    public void setCreate_rights(Boolean create_rights) {
        this.create_rights = create_rights;
    }

    public Boolean getDelete_rights() {
        return delete_rights;
    }

    public void setDelete_rights(Boolean delete_rights) {
        this.delete_rights = delete_rights;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }
}
