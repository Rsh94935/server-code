package com.dbserver.db.Services;

import com.dbserver.db.Mappers.ClientMapper;
import com.dbserver.db.Mappers.EventMapper;
import com.dbserver.db.Mappers.StaffMapper;
import com.dbserver.db.Objects.Client;
import com.dbserver.db.Objects.Event;
import com.dbserver.db.Objects.Staff;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class EventService {
    private final JdbcTemplate jdbcTemplate;

    public EventService() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        try {
            ds.setDriverClassName("com.mysql.jdbc.Driver");
        } catch (CannotGetJdbcConnectionException e) {
            e.printStackTrace();
        }
        ds.setUrl("jdbc:mysql://database-estate.cj0e3upnn9hj.eu-west-2.rds.amazonaws.com");
        ds.setUsername("");
        ds.setPassword("");
        this.jdbcTemplate = new JdbcTemplate(ds);
    }

    public Event createEvent(Event event, Integer staff_ID, String uuid) {
        StaffService ss = new StaffService();
        if ( !ss.confirmStaffIsLoggedIn(staff_ID, uuid, false) ) {
            return new Event();
        }
        String insert = "INSERT INTO `innodb`.`event_TBL` (`event_host_ID`, `clients_attending`, `start_time`, `end_time`, `event_name`, `event_description`, `street_no`, `street_name`, `town`, `county`, `post_code`, `price`, `location`, `images`) VALUES ('" + event.getEvent_host_ID() + "', '" + event.getClients_attending() +"', '" + event.getStart_time() + "', '" + event.getEnd_time() + "', '" + event.getEvent_name() + "', '" + event.getEvent_description() + "', '" + event.getStreet_no()+ "', '" + event.getStreet_name()+ "', '" + event.getTown()+ "', '" + event.getCounty()+ "', '" + event.getPost_code() + "', '" + event.getPrice() + "', '" + event.getLocation() + "', '" + event.getImages() + "')";
        this.jdbcTemplate.execute(insert);

        String get = "SELECT * FROM innodb.event_TBL ORDER BY event_ID DESC LIMIT 0, 1";
        List<Event> events = this.jdbcTemplate.query(get, new EventMapper());

        String getStaff = "SELECT * FROM innodb.Staff_TBL INNER JOIN innodb.internal_staff_TBL ON Staff_TBL.staff_ID=internal_staff_TBL.staff_ID WHERE internal = 'true' AND amend_rights = 'true'";
        List<Staff> addresses = this.jdbcTemplate.query(getStaff, new StaffMapper());
        String addressList = "";

        for ( Staff staff : addresses ) {
            if ( addressList.equals("") ) {
                addressList += staff.getEmail_address();
            } else {
                addressList += ", " + staff.getEmail_address();
            }
        }

        if ( !addressList.equals("") ) {
            Staff me = ss.getStaffById(staff_ID);
            EmailService es = new EmailService();
            StringBuilder message = new StringBuilder();
            message.append("New event has been created and requires approval: \n");
            message.append("\n Event ID: " + event.getEvent_ID());
            message.append("\n Event name: " + event.getEvent_name());
            message.append("\n Event description: " + event.getEvent_description());
            message.append("\n Event start time: " + new Date(event.getStart_time()));
            message.append("\n Event end time: " + new Date(event.getEnd_time()));
            message.append("\n Event created by: " + me.getEmail_address());
            es.sendEmail(addressList, "New event created", message.toString(), "");
        }

        return events.get(0);
    }

    public List<Event> getEventsRange(Integer start, Integer end, long currentDate) {
        String get = "SELECT * FROM innodb.event_TBL WHERE start_time > " + currentDate + " LIMIT " + start + ", " + end;
        List<Map<String, Object>> vals = this.jdbcTemplate.queryForList(get);

        return this.getEventList(vals);
    }

    public List<Event> getAuthEventsRange(Integer start, Integer end, long currentDate, String hPrice, String lPrice, String locationType, String searchLocation, Integer client_ID) {
        String get = "SELECT * FROM innodb.event_TBL WHERE start_time > " + currentDate + " AND staff_manager_ID IS NOT NULL AND (price BETWEEN " + lPrice + " AND " + hPrice + ") ";
        if ( !locationType.equals("") ) {
            get += "AND " + locationType + " = '" + searchLocation + "' ";
        }

        get += "LIMIT " + start + "," + end;

        List<Map<String, Object>> vals = this.jdbcTemplate.queryForList(get);
        List<Event> eventList = this.getEventList(vals);

        for (Event event : eventList) {
            String[] signedUp = event.getClients_attending().split(",");
            for (String client_signed_up : signedUp) {
                if ( client_signed_up.equals(client_ID.toString()) ) {
                    event.setClients_attending(client_ID.toString());
                    break;
                } else {
                    event.setClients_attending("");
                }
            }
        }

        return eventList;
    }

    public Event addClientToEvent(Integer event_ID, Integer client_ID, String uuid) {
        String get = "SELECT * FROM innodb.event_TBL WHERE event_ID = " + event_ID;

        List<Map<String, Object>> vals = this.jdbcTemplate.queryForList(get);
        Event event = this.getEventList(vals).get(0);
        String[] signedUp = event.getClients_attending().split(",");
        for (String client_signed_up : signedUp) {
            if ( client_signed_up.equals(client_ID.toString()) ) {
                return event;
            }
        }

        ClientService cs = new ClientService();
        if ( !cs.confirmClientIsLoggedIn(client_ID, uuid) ) {
            return event;
        }

        String clientListString = event.getClients_attending().equals("") ? client_ID.toString() : event.getClients_attending() + ", " + client_ID.toString();
        String update = "UPDATE innodb.event_TBL SET clients_attending = '" + clientListString + "' WHERE event_ID = " + event_ID;
        this.jdbcTemplate.execute(update);
        event.setClients_attending(client_ID.toString());

        return event;
    }

    public List<Event> getEventsInTown(String town, Integer start, Integer end) {
        String get = "SELECT * FROM innodb.event_TBL Where town = '" + town + "' LIMIT " + start + ", " + end;
        List<Map<String, Object>> vals = this.jdbcTemplate.queryForList(get);

        return this.getEventList(vals);
    }

    public List<Event> getEventsInCounty(String county, Integer start, Integer end) {
        String get = "SELECT * FROM innodb.event_TBL Where county = '" + county + "' LIMIT " + start + ", " + end;
        List<Map<String, Object>> vals = this.jdbcTemplate.queryForList(get);

        return this.getEventList(vals);
    }

    private List<Event> getEventList(List<Map<String, Object>> map) {
        List<Event> events = new ArrayList<>();

        for (Map val: map) {
            Event event = new Event();

            event.setEvent_ID((Integer) val.get("event_ID"));
            event.setStaff_manager_ID((Integer) val.get("staff_manager_ID"));
            event.setEvent_host_ID((Integer) val.get("event_host_ID"));
            event.setClients_attending((String) val.get("clients_attending"));
            event.setStart_time((long) val.get("start_time"));
            event.setEnd_time((long) val.get("end_time"));
            event.setEvent_name((String) val.get("event_name"));
            event.setEvent_description((String) val.get("event_description"));
            event.setStreet_no((String) val.get("street_no"));
            event.setStreet_name((String) val.get("street_name"));
            event.setTown((String) val.get("town"));
            event.setCounty((String) val.get("county"));
            event.setPost_code((String) val.get("post_code"));
            event.setPrice((String) val.get("price"));
            event.setLocation((String) val.get("location"));
            event.setImages((String) val.get("images"));

            events.add(event);
        }

        return events;
    }

    public List<Event> getUpcomingEvents(Integer staff_ID, String uuid, Boolean internalType, Integer start, Integer amount) {
        StaffService ss = new StaffService();
        if ( ss.confirmStaffIsLoggedIn(staff_ID, uuid, internalType) ) {
            long currentDate = new Date().getTime();
            String get = "SELECT * FROM innodb.event_TBL WHERE start_time > '" + currentDate + "' AND (staff_manager_ID = '" + staff_ID + "' OR event_host_ID = '" + staff_ID + "') LIMIT " + start + ", " + amount;
            List<Map<String, Object>> vals = this.jdbcTemplate.queryForList(get);

            return this.getEventList(vals);
        } else {
            return new ArrayList<>();
        }
    }

    public List<Event> getUnassignedEvents(Integer staff_ID, String uuid, Integer start, Integer amount) {
        StaffService ss = new StaffService();
        if ( ss.confirmStaffIsLoggedIn(staff_ID, uuid, true) ) {
            String get = "SELECT * FROM innodb.event_TBL WHERE staff_manager_ID IS NULL LIMIT " + start + ", " + amount;
            List<Map<String, Object>> vals = this.jdbcTemplate.queryForList(get);

            return this.getEventList(vals);
        } else {
            return new ArrayList<>();
        }
    }

    public Event addStaffManagerToEvent(Integer staff_ID, String uuid, Integer event_ID) {
        StaffService ss = new StaffService();
        if ( ss.confirmStaffHasRights(staff_ID, "amend_rights") && ss.confirmStaffIsLoggedIn(staff_ID, uuid, true) ) {
            String insertDate = "UPDATE innodb.event_TBL SET staff_manager_ID = '" + staff_ID + "' WHERE event_ID = " + event_ID;
            this.jdbcTemplate.execute(insertDate);

            String getClients = "SELECT * FROM innodb.Client_TBL WHERE subscribed = 'true'";
            List<Client> addresses = this.jdbcTemplate.query(getClients, new ClientMapper());
            String addressList = "";

            for ( Client client : addresses ) {
                if ( addressList.equals("") ) {
                    addressList += client.getEmail_address();
                } else {
                    addressList += ", " + client.getEmail_address();
                }
            }
            Event event = this.getEvent(event_ID);

            if ( !addressList.equals("") ) {
                Staff me = ss.getStaffById(staff_ID);
                Staff contact = ss.getStaffById(event.getEvent_host_ID());
                EmailService es = new EmailService();
                StringBuilder message = new StringBuilder();
                message.append("We would like to invite you to our new event coming soon: \n");
                message.append("\n Event name: " + event.getEvent_name());
                message.append("\n Event description: " + event.getEvent_description());
                message.append("\n Event address: " + event.getStreet_no() + " " + event.getStreet_name() + ", " + event.getTown() + ", " + event.getCounty() + ", " + event.getPost_code());
                message.append("\n Event start time: " + new Date(event.getStart_time()));
                message.append("\n Event end time: " + new Date(event.getEnd_time()));
                message.append("\n Event hosted by: " + contact.getEmail_address());
                message.append("\n Event manager: " + me.getEmail_address());
                es.sendEmail(addressList, "New event created", message.toString(), contact.getEmail_address());
            }

            return event;
        } else {
            return new Event();
        }
    }

    private Event getEvent(Integer event_ID) {
        String get = "SELECT * FROM innodb.event_TBL WHERE event_ID = " + event_ID;
        List<Map<String, Object>> eventList = this.jdbcTemplate.queryForList(get);

        return this.getEventList(eventList).get(0);
    }

    public String updateEvent(Map<String, Object> newEventDetails, Integer staff_ID, String uuid) {
        StaffService ss = new StaffService();
        if ( ss.confirmStaffIsLoggedIn(staff_ID, uuid, false) ) {
            for ( String key : newEventDetails.keySet() ) {
                if ( !newEventDetails.get(key).equals("") & !key.equals("event_ID") ) {
                    String update = "UPDATE innodb.Property_TBL Set " + key + " = '" + newEventDetails.get(key) + "' WHERE event_ID = " + newEventDetails.get("event_ID");
                    this.jdbcTemplate.execute(update);            }
            }
            return "Amendments saved";
        } else {
            return "Insufficient rights";
        }
    }

    public String deleteEvent(Integer staff_ID, String uuid, Integer event_ID, Boolean internalType) {
        StaffService ss = new StaffService();
        if ( ss.confirmStaffIsLoggedIn(staff_ID, uuid, internalType) ) {
            String delete = "DELETE FROM innodb.event_TBL WHERE event_ID = " + event_ID + " AND (staff_manager_ID = " + staff_ID + " OR event_host_ID = " + staff_ID + ")";
            this.jdbcTemplate.execute(delete);

            return "Event deleted";
        } else {
            return "Insufficient rights";
        }
    }

}
