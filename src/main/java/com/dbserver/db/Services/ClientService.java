package com.dbserver.db.Services;

import com.dbserver.db.Mappers.ClientMapper;
import com.dbserver.db.Mappers.PropertyMapper;
import com.dbserver.db.Mappers.StaffMapper;
import com.dbserver.db.Objects.Client;
import com.dbserver.db.Objects.Event;
import com.dbserver.db.Objects.Property;
import com.dbserver.db.Objects.Staff;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.validation.constraints.Email;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClientService {
    private final JdbcTemplate jdbcTemplate;

    public ClientService() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        try {
            ds.setDriverClassName("com.mysql.jdbc.Driver");
        } catch (CannotGetJdbcConnectionException e) {
            e.printStackTrace();
        }
        ds.setUrl("jdbc:mysql://database-estate.cj0e3upnn9hj.eu-west-2.rds.amazonaws.com");
        ds.setUsername("");
        ds.setPassword("");
        this.jdbcTemplate = new JdbcTemplate(ds);
    }

    public Client accessDB(Client client) {
        String insert = "INSERT INTO `innodb`.`Client_TBL` (`first_name`, `last_name`, `street_no`, `street_name`, `town`, `county`, `post_code`, `subscribed`, `email_address`, `pw`) VALUES ('" + client.getFirst_name() + "', '" + client.getLast_name() + "', '" + client.getStreet_no() +"', '" + client.getStreet_name() + "', '" + client.getTown() + "', '" + client.getCounty() + "', '" + client.getPost_code() + "', '" + client.getSubscribed() + "', '" + client.getEmail_address() + "', '" + client.getPw() + "')";
        this.jdbcTemplate.execute(insert);

        String get = "SELECT * FROM innodb.Client_TBL ORDER BY client_ID DESC LIMIT 0, 1";
        List<Client> newClient = this.jdbcTemplate.query(get, new ClientMapper());

        return newClient.get(0);
    }

    public List<Client> getClientList(Integer staff_ID, String uuid, Integer start, Integer amount) {
        StaffService ss = new StaffService();
        if( !ss.confirmStaffIsLoggedIn(staff_ID, uuid, true) ) {
            return new ArrayList<>();
        }

        String get = "SELECT * FROM innodb.Client_TBL LIMIT " + start + ", " + amount;
        List<Map<String, Object>> vals = this.jdbcTemplate.queryForList(get);

        return this.convertClientList(vals);
    }

    private List<Client> convertClientList(List<Map<String, Object>> map) {
        List<Client> clients = new ArrayList<>();

        for (Map val: map) {
            Client client = new Client();
            Boolean subscribed;
            String email_address;
            String pw;
            String uuid;
            long logged_in;

            client.setClient_ID((Integer) val.get("client_ID"));
            client.setFirst_name((String) val.get("first_name"));
            client.setLast_name((String) val.get("last_name"));
            client.setStreet_no((String) val.get("street_no"));
            client.setStreet_name((String) val.get("street_name"));
            client.setTown((String) val.get("town"));
            client.setCounty((String) val.get("county"));
            client.setPost_code((String) val.get("post_code"));
            client.setSubscribed(Boolean.parseBoolean((String) val.get("subscribed")));
            client.setEmail_address((String) val.get("email_address"));
            client.setPw((String) "");
            client.setUuid((String) val.get("uuid"));
            client.setLogged_in((long) 0);

            clients.add(client);
        }

        return clients;
    }

    public String contactClient(Integer staff_ID, String uuid, Integer client_ID, String subject, String message) {
        StaffService ss = new StaffService();
        if ( ss.confirmStaffIsLoggedIn(staff_ID, uuid, true) ) {
            Staff staff = ss.getStaffById(staff_ID);

            message += "\n\n Message from: "  + staff.getFirst_name() + " " + staff.getLast_name();

            String get = "SELECT * from innodb.Client_TBL WHERE client_ID = " + client_ID;
            List<Client> clientList = this.jdbcTemplate.query(get, new ClientMapper());

            Client client = clientList.get(0);

            EmailService es = new EmailService();
            es.sendEmail(client.getEmail_address(), subject, message, "");

            return "Email sent";
        } else {
            return "Insufficient rights";
        }
    }

    public String deleteClient(Integer clientID, Integer staff_ID, String uuid) {
        StaffService ss = new StaffService();
        if ( ss.confirmStaffIsLoggedIn(staff_ID, uuid, true) & ss.confirmStaffHasRights(staff_ID, "delete_rights") ) {

            String delete = "DELETE FROM innodb.Client_TBL WHERE client_ID = " + clientID;
            this.jdbcTemplate.execute(delete);

            return "Client deleted";
        }

        return "Insufficient rights";
    }

    public Boolean confirmClientIsLoggedIn(Integer client_ID, String UUID) {
        String get = "SELECT * FROM innodb.Client_TBL WHERE client_ID = ?";
        Client client = this.jdbcTemplate.queryForObject(get, new Object[]{client_ID}, new ClientMapper());

        return client.getUuid().equals(UUID);
    }
}
