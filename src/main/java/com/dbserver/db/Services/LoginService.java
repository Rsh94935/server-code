package com.dbserver.db.Services;

import com.dbserver.db.Mappers.ClientMapper;
import com.dbserver.db.Mappers.StaffMapper;
import com.dbserver.db.Objects.Client;
import com.dbserver.db.Objects.Staff;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.Date;

public class LoginService {
    private final JdbcTemplate jdbcTemplate;

    public LoginService() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        try {
            ds.setDriverClassName("com.mysql.jdbc.Driver");
        } catch (CannotGetJdbcConnectionException e) {
            e.printStackTrace();
        }
        ds.setUrl("jdbc:mysql://database-estate.cj0e3upnn9hj.eu-west-2.rds.amazonaws.com");
        ds.setUsername("");
        ds.setPassword("");
        this.jdbcTemplate = new JdbcTemplate(ds);
    }

    public Client clientLogin(String username, String pw) throws Exception{
        String get = "SELECT * FROM innodb.Client_TBL WHERE email_address = ?";
        Client val = this.jdbcTemplate.queryForObject(get, new Object[]{username}, new ClientMapper());

        if ( val.getPw().equals(pw) ) {
            String uuid = java.util.UUID.randomUUID().toString();
            long currentDate = new Date().getTime();
            String insertUUID = "UPDATE innodb.Client_TBL SET uuid = '" + uuid + "' WHERE client_Id = " + val.getClient_ID();
            this.jdbcTemplate.execute(insertUUID);
            val.setUuid(uuid);

            String insertDate = "UPDATE innodb.Client_TBL SET logged_in = '" + currentDate + "' WHERE client_Id = " + val.getClient_ID();
            this.jdbcTemplate.execute(insertDate);
            val.setLogged_in(currentDate);
            val.setPw("");

            return val;
        } else {
            return new Client();
        }
    }

    public Client loggedInCheck(String uuid) throws Exception {
        String get = "SELECT * FROM innodb.Client_TBL WHERE uuid = ?";
        Client val = this.jdbcTemplate.queryForObject(get, new Object[]{uuid}, new ClientMapper());
        long currentDate = new Date().getTime();
        Client retVal = new Client();

        long diff = currentDate - val.getLogged_in();

        if ( diff < 129600000 ) {
            val.setPw("");
            retVal = val;
        }

        return retVal;
    }

    public Staff staffLogin(String username, String pw) throws Exception {
        String get = "SELECT * FROM innodb.Staff_TBL WHERE email_address = ?";
        Staff val = this.jdbcTemplate.queryForObject(get, new Object[]{username}, new StaffMapper());

        if ( val.getPw().equals(pw) ) {
            String uuid = java.util.UUID.randomUUID().toString();
            long currentDate = new Date().getTime();
            String insertUUID = "UPDATE innodb.Staff_TBL SET sessionUUID = '" + uuid + "' WHERE staff_ID = " + val.getStaff_ID();
            this.jdbcTemplate.execute(insertUUID);
            val.setSessionUUID(uuid);

            String insertDate = "UPDATE innodb.Staff_TBL SET logged_in = '" + currentDate + "' WHERE staff_ID = " + val.getStaff_ID();
            this.jdbcTemplate.execute(insertDate);
            val.setLogged_in(currentDate);

            val.setPw("");

            return val;
        } else {
            return new Staff();
        }
    }

    public Staff staffLoggedInCheck(String uuid) throws Exception {
        String get = "SELECT * FROM innodb.Staff_TBL WHERE sessionUUID = ?";
        Staff val = this.jdbcTemplate.queryForObject(get, new Object[]{uuid}, new StaffMapper());
        long currentDate = new Date().getTime();
        Staff retVal = new Staff();

        long diff = currentDate - val.getLogged_in();

        if ( diff < 129600000 ) {
            val.setPw("");
            retVal = val;
        }

        return retVal;
    }
}
