package com.dbserver.db.Services;

import com.dbserver.db.Mappers.InternalStaffMapper;
import com.dbserver.db.Mappers.PropertyMapper;
import com.dbserver.db.Mappers.SaleMapper;
import com.dbserver.db.Objects.Property;
import com.dbserver.db.Objects.Sale;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PropertyService {
    private final JdbcTemplate jdbcTemplate;

    public PropertyService() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        try {
            ds.setDriverClassName("com.mysql.jdbc.Driver");
        } catch (CannotGetJdbcConnectionException e) {
            e.printStackTrace();
        }
        ds.setUrl("jdbc:mysql://database-estate.cj0e3upnn9hj.eu-west-2.rds.amazonaws.com");
        ds.setUsername("");
        ds.setPassword("");
        this.jdbcTemplate = new JdbcTemplate(ds);
    }

    public Property createProperty(Property property) {
        String insert = "INSERT INTO `innodb`.`Property_TBL` (`street_no`, `street_name`, `town`, `county`, `post_code`, `description`, `images`, `location`, `price`, `attachments`) VALUES ('" + property.getStreet_no() + "', '" + property.getStreet_name() + "', '" + property.getTown() + "', '" + property.getCounty() + "', '" + property.getPost_code() + "', ' " + property.getDescription() + "', '" + property.getImages() + "', '" + property.getLocation() + "', '" + property.getPrice() + "', '" + property.getAttachments() + "')";
        this.jdbcTemplate.execute(insert);

        String get = "SELECT * FROM innodb.Property_TBL ORDER BY property_ID DESC LIMIT 0, 1";
        List<Property> prop = this.jdbcTemplate.query(get, new PropertyMapper());



        return prop.get(0);
    }

    public List<Property> getAllProperties() {
        String get = "SELECT * FROM innodb.Property_TBL";
        List<Property> propertyList = this.jdbcTemplate.query(get, new PropertyMapper());

        return propertyList;
    }

    public List<Property> getPropertyRange(Integer from, Integer to, String hPrice, String lPrice, String locationType, String searchLocation) {
        String get = "SELECT * FROM innodb.Property_TBL WHERE (price BETWEEN " + lPrice + " AND " + hPrice + ") ";
        if ( !locationType.equals("") ) {
            get += "AND " + locationType + " = '" + searchLocation + "' ";
        }

        get += "LIMIT " + from + "," + to;

        List<Property> propertyList = this.jdbcTemplate.query(get, new PropertyMapper());

        return propertyList;
    }

    public List<Property> getAvailablePropertyRange(Integer from, Integer to, String hPrice, String lPrice, String locationType, String searchLocation) {
        String getHomes = "SELECT * From innodb.Sale_TBL INNER JOIN innodb.Property_TBL ON Sale_TBL.property_ID=Property_TBL.property_ID WHERE buyer_ID = 0 AND (price BETWEEN " + lPrice + " AND " + hPrice + ") ";
        if ( !locationType.equals("") ) {
            getHomes += "AND " + locationType + " = '" + searchLocation + "' ";
        }

        getHomes += "LIMIT " + from + "," + to;
        List<Map<String, Object>> returnSales = this.jdbcTemplate.queryForList(getHomes);

        return this.getPropertyList(returnSales);
    }

    public List<Property> getPropertyList(List<Map<String, Object>> map) {
        List<Property> properties = new ArrayList<>();

        for (Map val: map) {
            Property property = new Property();

            property.setProperty_ID((Integer) val.get("property_ID"));
            property.setStreet_no((String) val.get("street_no"));
            property.setStreet_name((String) val.get("street_name"));
            property.setTown((String) val.get("town"));
            property.setCounty((String) val.get("county"));
            property.setPost_code((String) val.get("post_code"));
            property.setDescription((String) val.get("description"));
            property.setImages((String) val.get("images"));
            property.setLocation((String) val.get("location"));
            property.setPrice((String) val.get("price"));
            property.setAttachments((String) val.get("attachments"));

            properties.add(property);
        }

        return properties;
    }

    public void updatePropertyDetails(Map<String, Object> newPropertyDetails) {
        for ( String key : newPropertyDetails.keySet() ) {
            if ( !newPropertyDetails.get(key).equals("") & !key.equals("property_ID") ) {
                String update = "UPDATE innodb.Property_TBL Set " + key + " = '" + newPropertyDetails.get(key) + "' WHERE property_ID = " + newPropertyDetails.get("property_ID");
                this.jdbcTemplate.execute(update);            }
        }
    }

    public String deleteProperty(Integer propertyId) {
        String delete = "DELETE FROM innodb.Property_TBL WHERE property_ID = " + propertyId;
        this.jdbcTemplate.execute(delete);

        return "Property deleted";
    }

}
