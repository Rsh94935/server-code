package com.dbserver.db.Services;

import com.dbserver.db.Mappers.ClientMapper;
import com.dbserver.db.Mappers.ContactMapper;
import com.dbserver.db.Mappers.InternalStaffMapper;
import com.dbserver.db.Mappers.StaffMapper;
import com.dbserver.db.Objects.*;
import com.sun.javafx.collections.MappingChange;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.*;

public class StaffService {
    private final JdbcTemplate jdbcTemplate;

    public StaffService() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        try {
            ds.setDriverClassName("com.mysql.jdbc.Driver");
        } catch (CannotGetJdbcConnectionException e) {
            e.printStackTrace();
        }
        ds.setUrl("jdbc:mysql://database-estate.cj0e3upnn9hj.eu-west-2.rds.amazonaws.com");
        ds.setUsername("");
        ds.setPassword("");
        this.jdbcTemplate = new JdbcTemplate(ds);
    }

    public Staff createStaff(Staff staff) {
        String insert = "INSERT INTO `innodb`.`Staff_TBL` (`first_name`, `last_name`, `email_address`, `pw`, `internal`) VALUES ('" + staff.getFirst_name() + "', '" + staff.getLast_name() + "', '" + staff.getEmail_address() + "', '" + staff.getPw() + "', '" + staff.getInternal() + "')";
        this.jdbcTemplate.execute(insert);

        String get = "SELECT * FROM innodb.Staff_TBL WHERE email_address = ?";
        Staff val = this.jdbcTemplate.queryForObject(get, new Object[]{staff.getEmail_address()}, new StaffMapper());

        if ( staff.getInternal().equals(true) ) {
            String internal = "INSERT INTO `innodb`.`internal_staff_TBL` (`staff_ID`, `read_rights`, `amend_rights`, `create_rights`, `delete_rights`, `admin`) VALUES ('" + val.getStaff_ID() + "', 'true', 'false', 'false', 'false', 'false')";
            this.jdbcTemplate.execute(internal);
        } else {
            String contact = "INSERT INTO `innodb`.`Contact_TBL` (`staff_ID`, `company_name`, `hourly_rate`) VALUES ('" + val.getStaff_ID() + "', '', '0.00')";
           this.jdbcTemplate.execute(contact);
        }

        return val;
    }


    public List<Staff> getAllStaff() {
        String get = "SELECT * FROM innodb.Staff_TBL";
        List<Staff> vals = this.jdbcTemplate.query(get, new StaffMapper());

        return vals;
    }

    public List<Staff> getStaffBasedOnInternal(Integer staff_ID, String uuid, Integer start, Integer amount) {
        if ( this.confirmStaffIsLoggedIn(staff_ID, uuid, true) ) {
            String get = "SELECT * FROM innodb.Staff_TBL LIMIT " + start + ", " + amount;
            List<Map<String, Object>> vals = this.jdbcTemplate.queryForList(get);

            return this.getStaffList(vals);
        } else {
            return new ArrayList<>();
        }
    }

    private List<Staff> getStaffList(List<Map<String, Object>> map) {
        List<Staff> staffList = new ArrayList<>();

        for (Map val: map) {
            Staff staff = new Staff();

            staff.setStaff_ID((Integer) val.get("staff_ID"));
            staff.setFirst_name((String) val.get("first_name"));
            staff.setLast_name((String) val.get("last_name"));
            staff.setEmail_address((String) val.get("email_address"));
            staff.setPw("");
            staff.setInternal(Boolean.parseBoolean((String) val.get("internal")));
            staff.setSessionUUID("");
            staff.setLogged_in((long) 0);

            staffList.add(staff);
        }

        return staffList;
    }

    public Object getStaffDetails(Boolean internalType, Integer staff_ID, String uuid, Integer search_ID, Boolean searchInternalType) {
        Object val;
        if ( searchInternalType ) {
            if (this.confirmStaffIsLoggedIn(staff_ID, uuid, true)) {
                String get = "SELECT * FROM innodb.internal_staff_TBL WHERE staff_ID = ?";
                val = this.jdbcTemplate.queryForObject(get, new Object[]{search_ID}, new InternalStaffMapper());
            } else {
                return new InternalStaff();
            }
        } else {
            if ( this.confirmStaffIsLoggedIn(staff_ID, uuid, internalType) ) {
                String get = "SELECT * FROM innodb.Contact_TBL WHERE staff_ID = ?";
                Contact contact = this.jdbcTemplate.queryForObject(get, new Object[]{search_ID}, new ContactMapper());
                val = contact;
                if ( !internalType ) {
                    if ( this.confirmContactIsOffCompany(staff_ID, contact.getCompany_name()) ) {

                    }
                }
            } else {
                return new InternalStaff();
            }
        }

        return val;
    }

    public void updateStaffDetails(Map<String, Object> newStaffDetails, String table) {
        for ( String key : newStaffDetails.keySet() ) {
            if ( !newStaffDetails.get(key).equals("") & !key.equals("staff_ID") ) {
                String update = "UPDATE innodb." + table + " Set " + key + " = '" + newStaffDetails.get(key) + "' WHERE staff_ID = " + newStaffDetails.get("staff_ID");
                this.jdbcTemplate.execute(update);            }
        }
    }

    public String deleteStaff(Integer staff_ID, Boolean internalType, Integer delete_ID, Boolean delete_type) {

        String typeDelete = "";

        if ( delete_type ) {
            if ( this.confirmStaffHasRights(staff_ID, "delete_rights") ) {
                typeDelete = "DELETE FROM innodb.internal_staff_TBL WHERE staff_ID = " + delete_ID;
            } else {
                return "Insufficient rights";
            }
        } else {
            if ( internalType ) {
                if ( !this.confirmStaffHasRights(staff_ID, "delete_rights") ) {
                    return "Insufficient rights";
                }
            }
            typeDelete = "DELETE FROM innodb.Contact_TBL WHERE staff_ID = " + delete_ID;
        }

        this.jdbcTemplate.execute(typeDelete);
        String delete = "DELETE FROM innodb.Staff_TBL WHERE staff_ID = " + delete_ID;
        this.jdbcTemplate.execute(delete);

        return "Staff member deleted";
    }

    public Boolean confirmStaffIsLoggedIn(Integer staff_ID, String UUID, Boolean internalType) {
        String get = "SELECT * FROM innodb.Staff_TBL WHERE staff_ID = ?";
        Staff staff = this.jdbcTemplate.queryForObject(get, new Object[]{staff_ID}, new StaffMapper());

        return staff.getSessionUUID().equals(UUID) && staff.getInternal().equals(internalType);
    }

    public Boolean confirmStaffHasRights(Integer staff_ID, String rights) {
        String getRights = "SELECT * FROM innodb.internal_staff_TBL WHERE staff_ID = ?";
        InternalStaff IS = this.jdbcTemplate.queryForObject(getRights, new Object[]{staff_ID}, new InternalStaffMapper());

        if ( IS.getAdmin() ) {
            return true;
        }

        switch(rights) {
            case "read_rights":
                return IS.getRead_rights();
            case "amend_rights":
                return IS.getAmend_rights();
            case "create_rights":
                return IS.getCreate_rights();
            case "delete_rights":
                return IS.getDelete_rights();
        }

        return false;
    }

    public Boolean confirmContactIsOffCompany(Integer staff_ID, String companyName) {
        String get = "SELECT * FROM innodb.Contact_TBL WHERE staff_ID = ?";
        Contact contact = this.jdbcTemplate.queryForObject(get, new Object[]{staff_ID}, new ContactMapper());

        return contact.getCompany_name().equals(companyName);
    }

    public Staff getStaffById(Integer staff_ID) {
        String get = "SELECT * FROM innodb.Staff_TBL WHERE staff_ID = " + staff_ID;
        List<Staff> staffList = this.jdbcTemplate.query(get, new StaffMapper());

        return staffList.get(0);
    }

}
