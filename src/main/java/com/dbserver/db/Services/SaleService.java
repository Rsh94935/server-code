package com.dbserver.db.Services;

import com.dbserver.db.Mappers.InternalStaffMapper;
import com.dbserver.db.Mappers.PropertyMapper;
import com.dbserver.db.Mappers.SaleMapper;
import com.dbserver.db.Objects.Event;
import com.dbserver.db.Objects.Property;
import com.dbserver.db.Objects.Sale;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SaleService {
    private final JdbcTemplate jdbcTemplate;

    public SaleService() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        try {
            ds.setDriverClassName("com.mysql.jdbc.Driver");
        } catch (CannotGetJdbcConnectionException e) {
            e.printStackTrace();
        }
        ds.setUrl("jdbc:mysql://database-estate.cj0e3upnn9hj.eu-west-2.rds.amazonaws.com");
        ds.setUsername("");
        ds.setPassword("");
        this.jdbcTemplate = new JdbcTemplate(ds);
    }

    public Sale createSale(Sale sale) {
        String insert = "INSERT INTO `innodb`.`Sale_TBL` (`property_ID`, `seller_ID`, `buyer_ID`, `staff_ID`, `sale_price`) VALUES ('" + sale.getProperty_ID() + "', '" + sale.getSeller_ID() + "', '0', '" + sale.getStaff_ID() + "', '0.00')";
        this.jdbcTemplate.execute(insert);

        String get = "SELECT * FROM innodb.Sale_TBL ORDER BY sale_ID DESC LIMIT 0, 1";
        List<Sale> newSale = this.jdbcTemplate.query(get, new SaleMapper());

        return newSale.get(0);
    }

    public List<Sale> getMySales(Integer staff_ID, String uuid, Integer start, Integer amount) {
        StaffService ss = new StaffService();
        if ( ss.confirmStaffIsLoggedIn(staff_ID, uuid, true) ) {
            String get = "SELECT * FROM innodb.event_TBL WHERE staff_manager_ID = '0' LIMIT " + start + ", " + amount;
            List<Map<String, Object>> vals = this.jdbcTemplate.queryForList(get);

            return this.getSaleList(vals);
        } else {
            return new ArrayList<>();
        }
    }

    public List<Sale> getSaleList(List<Map<String, Object>> map) {
        List<Sale> sales = new ArrayList<>();

        for (Map val: map) {
            Sale sale = new Sale();

            sale.setSale_ID((Integer) val.get("sale_ID"));
            sale.setProperty_ID((Integer) val.get("property_ID"));
            sale.setSeller_ID((Integer) val.get("seller_ID"));
            sale.setBuyer_ID((Integer) val.get("buyer_ID"));
            sale.setStaff_ID((Integer) val.get("staff_ID"));
            sale.setSale_price((String) val.get("sale_price"));

            sales.add(sale);
        }

        return sales;
    }

    public Sale getSale(Integer sale_ID) {
        String get = "SELECT * FROM innodb.Sale_TBL WHERE sale_ID = ?";
        Sale val = this.jdbcTemplate.queryForObject(get, new Object[]{sale_ID}, new SaleMapper());

        return val;
    }

    public void updateSaleDetails(Map<String, Object> newSaleDetails) {
        for ( String key : newSaleDetails.keySet() ) {
            if ( !newSaleDetails.get(key).equals("") & !key.equals("property_ID") ) {
                String update = "UPDATE innodb.Sale_TBL Set " + key + " = '" + newSaleDetails.get(key) + "' WHERE Sale_ID = " + newSaleDetails.get("Sale_ID");
                this.jdbcTemplate.execute(update);            }
        }
    }

    public String deleteSale(Integer sale_ID) {
        String delete = "DELETE FROM innodb.Sale_TBL WHERE sale_ID = " + sale_ID;
        this.jdbcTemplate.execute(delete);

        return "Sale deleted";
    }
}
