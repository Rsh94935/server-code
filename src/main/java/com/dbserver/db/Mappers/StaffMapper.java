package com.dbserver.db.Mappers;

import com.dbserver.db.Objects.Staff;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StaffMapper implements RowMapper<Staff> {

    @Override
    public Staff mapRow(ResultSet rs, int rowNum) throws SQLException {
        Staff staff = new Staff();
        staff.setStaff_ID(rs.getInt("staff_ID"));
        staff.setFirst_name(rs.getString("first_name"));
        staff.setLast_name(rs.getString("last_name"));
        staff.setEmail_address(rs.getString("email_address"));
        staff.setPw(rs.getString("pw"));
        staff.setInternal(rs.getBoolean("internal"));
        staff.setSessionUUID(rs.getString("sessionUUID"));
        staff.setLogged_in(rs.getLong("logged_in"));

        return staff;
    }
}
