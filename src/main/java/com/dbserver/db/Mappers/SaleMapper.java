package com.dbserver.db.Mappers;

import com.dbserver.db.Objects.Property;
import com.dbserver.db.Objects.Sale;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SaleMapper implements RowMapper<Sale> {

    @Override
    public Sale mapRow(ResultSet rs, int rowNum) throws SQLException {
        Sale sale = new Sale();
        sale.setSale_ID(rs.getInt("sale_ID"));
        sale.setProperty_ID(rs.getInt("property_ID"));
        sale.setSeller_ID(rs.getInt("seller_ID"));
        sale.setBuyer_ID(rs.getInt("buyer_ID"));
        sale.setStaff_ID(rs.getInt("staff_ID"));
        sale.setSale_price(rs.getString("sale_price"));

        return sale;
    }
}
