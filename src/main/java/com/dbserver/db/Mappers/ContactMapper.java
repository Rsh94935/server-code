package com.dbserver.db.Mappers;

import com.dbserver.db.Objects.Contact;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ContactMapper implements RowMapper<Contact> {

    @Override
    public Contact mapRow(ResultSet rs, int rowNum) throws SQLException {
        Contact contact = new Contact();
        contact.setStaff_ID(rs.getInt("staff_ID"));
        contact.setCompany_name(rs.getString("company_name"));
        contact.setHourly_rate(rs.getString("hourly_rate"));

        return contact;
    }
}
