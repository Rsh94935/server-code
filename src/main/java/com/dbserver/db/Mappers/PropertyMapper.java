package com.dbserver.db.Mappers;

import com.dbserver.db.Objects.InternalStaff;
import com.dbserver.db.Objects.Property;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PropertyMapper implements RowMapper<Property> {

    @Override
    public Property mapRow(ResultSet rs, int rowNum) throws SQLException {
        Property property = new Property();
        property.setProperty_ID(rs.getInt("property_ID"));
        property.setStreet_no(rs.getString("street_no"));
        property.setStreet_name(rs.getString("street_name"));
        property.setTown(rs.getString("town"));
        property.setCounty(rs.getString("county"));
        property.setPost_code(rs.getString("post_code"));
        property.setDescription(rs.getString("description"));
        property.setImages(rs.getString("images"));
        property.setLocation(rs.getString("location"));
        property.setPrice(rs.getString("price"));
        property.setAttachments(rs.getString("attachments"));

        return property;
    }
}
