package com.dbserver.db.Mappers;

import com.dbserver.db.Objects.Client;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientMapper implements RowMapper<Client> {

    @Override
    public Client mapRow(ResultSet rs, int rowNum) throws SQLException {
        Client client = new Client();
        client.setClient_ID(rs.getInt("client_ID"));
        client.setFirst_name(rs.getString("first_name"));
        client.setLast_name(rs.getString("last_name"));
        client.setStreet_no(rs.getString("street_no"));
        client.setStreet_name(rs.getString("street_name"));
        client.setTown(rs.getString("town"));
        client.setCounty(rs.getString("county"));
        client.setPost_code(rs.getString("post_code"));
        client.setSubscribed(rs.getBoolean("subscribed"));
        client.setEmail_address(rs.getString("email_address"));
        client.setPw(rs.getString("pw"));
        client.setUuid(rs.getString("uuid"));
        client.setLogged_in(rs.getLong("logged_in"));

        return client;
    }
}
