package com.dbserver.db.Mappers;

import com.dbserver.db.Objects.Event;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EventMapper implements RowMapper<Event> {

    @Override
    public Event mapRow(ResultSet rs, int rowNum) throws SQLException {
        Event event = new Event();

        event.setEvent_ID(rs.getInt("event_ID"));
        event.setStaff_manager_ID(rs.getInt("staff_manager_ID"));
        event.setEvent_host_ID(rs.getInt("event_host_ID"));
        event.setClients_attending(rs.getString("clients_attending"));
        event.setStart_time(rs.getLong("start_time"));
        event.setEnd_time(rs.getLong("end_time"));
        event.setEvent_name(rs.getString("event_name"));
        event.setEvent_description(rs.getString("event_description"));
        event.setStreet_no(rs.getString("street_no"));
        event.setStreet_name(rs.getString("street_name"));
        event.setTown(rs.getString("town"));
        event.setCounty(rs.getString("county"));
        event.setPost_code(rs.getString("post_code"));
        event.setPrice(rs.getString("price"));
        event.setLocation(rs.getString("location"));
        event.setImages(rs.getString("images"));

        return event;
    }
}
