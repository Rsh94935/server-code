package com.dbserver.db.Mappers;

import com.dbserver.db.Objects.ServiceDetails;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ServiceDetailsMapper implements RowMapper<ServiceDetails> {

    @Override
    public ServiceDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
        ServiceDetails serviceDetails = new ServiceDetails();
        serviceDetails.setService_details_ID(rs.getInt("service_details_ID"));
        serviceDetails.setName(rs.getString("name"));
        serviceDetails.setDescription(rs.getString("description"));
        serviceDetails.setStreet_no(rs.getString("street_no"));
        serviceDetails.setStreet_name(rs.getString("street_name"));
        serviceDetails.setTown(rs.getString("town"));
        serviceDetails.setCounty(rs.getString("county"));
        serviceDetails.setPost_code(rs.getString("post_code"));
        serviceDetails.setPrice(rs.getString("price"));
        serviceDetails.setImages(rs.getString("images"));
        serviceDetails.setLocation(rs.getString("location"));
        serviceDetails.setService_provider(rs.getInt("service_provider"));
        serviceDetails.setDateStart(rs.getLong("dateStart"));
        serviceDetails.setDateEnd(rs.getLong("dateEnd"));
        serviceDetails.setHostingCompany(rs.getString("hostingCompany"));

        return serviceDetails;
    }
}
