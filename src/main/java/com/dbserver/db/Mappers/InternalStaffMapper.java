package com.dbserver.db.Mappers;

import com.dbserver.db.Objects.InternalStaff;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InternalStaffMapper implements RowMapper<InternalStaff> {

    @Override
    public InternalStaff mapRow(ResultSet rs, int rowNum) throws SQLException {
        InternalStaff internalStaff = new InternalStaff();
        internalStaff.setStaff_ID(rs.getInt("staff_ID"));
        internalStaff.setRead_rights(rs.getBoolean("read_rights"));
        internalStaff.setAmend_rights(rs.getBoolean("amend_rights"));
        internalStaff.setCreate_rights(rs.getBoolean("create_rights"));
        internalStaff.setDelete_rights(rs.getBoolean("delete_rights"));
        internalStaff.setAdmin(rs.getBoolean("admin"));

        return internalStaff;
    }
}
